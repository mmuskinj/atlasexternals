# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  URLLIB3_PYTHON_PATH
#
# Can be steered by URLLIB3_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If it was already found, let's be quiet:
if( URLLIB3_FOUND )
   set( urllib3_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( URLLIB3_ROOT )
   set( _extraUrllib3Args NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the python path:
find_path( URLLIB3_PYTHON_PATH 
   NAMES urllib3/__init__.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${URLLIB3_ROOT}
   ${_extraUrllib3Args} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( urllib3 DEFAULT_MSG
   URLLIB3_PYTHON_PATH )

# Set up the RPM dependency:
lcg_need_rpm( urllib3 )

# Clean up:
if( _extraUrllib3Args )
   unset( _extraUrllib3Args )
endif()
