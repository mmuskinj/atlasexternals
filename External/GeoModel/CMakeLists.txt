# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building GeoModel Kernel for ATLAS.
#

cmake_minimum_required( VERSION 3.7 )

# The name of the package:
atlas_subdir( GeoModel )

# In release recompilation mode finish here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

if( ATLAS_BUILD_EIGEN )
   set( EIGEN_INCLUDE_DIRS "${CMAKE_INCLUDE_OUTPUT_DIRECTORY}/eigen3" )
else()
   find_package( Eigen 3.0.5 REQUIRED )
endif()

set( _extraOptions )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
    list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 11 )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

# Directory for the temporary build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/GeoModelBuild )

# Set up the build of GeoModel:
ExternalProject_Add( GeoModel
   PREFIX ${CMAKE_BINARY_DIR}
   GIT_REPOSITORY https://gitlab.cern.ch/GeoModelDev/GeoModelKernel.git
   GIT_TAG eccf7ed2
   CMAKE_CACHE_ARGS 
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DEIGEN3_INCLUDE_DIR:PATH=${EIGEN_INCLUDE_DIRS}
   ${_extraOptions}
   LOG_CONFIGURE 1
   )

add_dependencies( Package_GeoModel GeoModel )

if( ATLAS_BUILD_EIGEN )
  add_dependencies ( GeoModel Eigen )
endif()

# Install GeoModel:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FindGeoModel.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
