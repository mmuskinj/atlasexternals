# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Package building the dSFMT libraries as part of the ATLAS offline software
# build.
#

# The name of the package:
atlas_subdir( dSFMT )

# In release recompilation mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Directory to put the intermediate build results in:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/dSFMTBuild )

# Flags for the build:
set( _flags CMTCONFIG=${ATLAS_PLATFORM} CC=${CMAKE_C_COMPILER}
   AR=${CMAKE_AR} OUTDIR=${_buildDir} )

# Build the package for the build area:
ExternalProject_Add( dSFMT
   PREFIX ${CMAKE_BINARY_DIR}
   URL ${CMAKE_CURRENT_SOURCE_DIR}/src/dSFMT-src-2.1.tar.gz
   URL_MD5 b3a38dac7fd8996a70d02edc4432dd75
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   PATCH_COMMAND ${CMAKE_COMMAND} -E copy
   ${CMAKE_CURRENT_SOURCE_DIR}/src/Makefile.atlas
   <SOURCE_DIR>/Makefile
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E echo
   "Configuring the build of dSFMT"
   BUILD_COMMAND make std-check ${_flags}
   COMMAND make sse2-check ${_flags}
   COMMAND make all ${_flags}
   INSTALL_COMMAND ${CMAKE_COMMAND} -E make_directory ${_buildDir}/include
   COMMAND ${CMAKE_COMMAND} -E make_directory ${_buildDir}/lib
   COMMAND make install ${_flags}
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   BUILD_IN_SOURCE 1 )
add_dependencies( Package_dSFMT dSFMT )

# Install it:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install its find-module:
install( FILES cmake/FinddSFMT.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )
