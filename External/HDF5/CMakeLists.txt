# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# Package building HDF5 for the offline/analysis releases.
#

# The name of the package:
atlas_subdir( HDF5 )

# Set the CMake version required by HDF5.
cmake_minimum_required( VERSION 3.10 )

# Temporary directory for the build results:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HDF5Build" )

# Find the external(s) needed for the build:
find_package( ZLIB REQUIRED )
list( GET ZLIB_INCLUDE_DIRS 0 _zlibInclude )
list( GET ZLIB_LIBRARIES    0 _zlibLibrary )

# Optional argument(s):
set( _extraConf )
if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraConf -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Build HDF5:
ExternalProject_Add( HDF5
   PREFIX ${CMAKE_BINARY_DIR}
   URL http://cern.ch/atlas-software-dist-eos/externals/hdf5/hdf5-1.10.3.tar.gz
   URL_MD5 5709332720fc14d55eafe296dab2dfbd
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DHDF5_BUILD_CPP_LIB:BOOL=ON
   -DBUILD_SHARED_LIBS:BOOL=ON
   -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=ON
   -DZLIB_LIBRARY:FILEPATH=${_zlibLibrary}
   -DZLIB_INCLUDE_DIR:PATH=${_zlibInclude}
   ${_extraConf}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( HDF5 purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for HDF5"
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( HDF5 buildinstall
   COMMAND ${CMAKE_COMMAND} -E remove -f ${_buildDir}/lib/libdynlib*
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing HDF5 into the build area"
   DEPENDEES install )
add_dependencies( Package_HDF5 HDF5 )

# Install HDF5:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
