# Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#
# This file is here to intercept find_package(Qt5) calls, and
# massage the paths produced by the system module, to make them relocatable.
#

# The LCG include(s):
include( LCGFunctions )

# Find the external(s) needed for the build:
find_package( Freetype ) # this is needed at runtime, otherwise the old lib on Lxplus is taken

# Qt5 needs at least one component to be requested from it. So let's make
# Core compulsory.
list( APPEND Qt5_FIND_COMPONENTS Core )
list( REMOVE_DUPLICATES Qt5_FIND_COMPONENTS )
set( _qt5_components ${Qt5_FIND_COMPONENTS} )

# Temporarily clean out CMAKE_MODULE_PATH, so that we could pick up
# the CMake code from inside the Qt5 installation itself:
set( _modulePathBackup ${CMAKE_MODULE_PATH} )
set( CMAKE_MODULE_PATH )

# Let Qt's own CMake code be found:
find_package( Qt5 )

# Restore CMAKE_MODULE_PATH:
set( CMAKE_MODULE_PATH ${_modulePathBackup} )
unset( _modulePathBackup )
set( CMAKE_SYSTEM_IGNORE_PATH )

# Massage the paths found by the system module:
if( Qt5_FOUND AND NOT GAUDI_ATLAS )

   # The include and library directories are set up, exceptionally, as cache
   # variables for this module. That's because the include directories are
   # set a bit inconsistently by the Qt CMake code. (It only sets them up on
   # the first find_package(...) call.)

   # Make sure that the lib/ directory is added to the environment:
   set( _relocatableLibDir "${QT5_ROOT}/lib" )
   _lcg_make_paths_relocatable( _relocatableLibDir )
   set( QT5_LIBRARY_DIRS
      $<BUILD_INTERFACE:${QT5_ROOT}/lib>
      $<INSTALL_INTERFACE:${_relocatableLibDir}>
      CACHE PATH "Library directories for Qt5" )
   unset( _relocatableLibDir )

   # Make sure that the include directories are added to the environment:
   set( _incDirs )
   foreach( component ${_qt5_components} )
      foreach( _inc ${Qt5${component}_INCLUDE_DIRS} )
         set( _relocatableIncDir ${_inc} )
         _lcg_make_paths_relocatable( _relocatableIncDir )
         list( APPEND _incDirs
            $<BUILD_INTERFACE:${_inc}>
            $<INSTALL_INTERFACE:${_relocatableIncDir}> )
         unset( _relocatableIncDir )
      endforeach()
   endforeach()
   set( QT5_INCLUDE_DIRS ${_incDirs}
      CACHE PATH "Include directories for Qt5" )
   unset( _incDirs )
endif()


# Set the environment variables needed for the Qt5 runtime:
# -- Set the QT_PLUGIN_PATH environment variable:
if( Qt5_FOUND )
    
    # get the location of the Qt5Core lib and set some variables
    get_target_property( QtCore_location Qt5::Core LOCATION )
    set( _version ${QT5_VERSION} )

    # set the env vars needed at runtime
    set( QT5_ENVIRONMENT 
        PREPEND QT_PLUGIN_PATH ${QT5_ROOT}/plugins
        PREPEND QTDIR ${QT5_ROOT}
        PREPEND QTINC ${QT5_ROOT}/include
        PREPEND QTLIB ${QT5_ROOT}/lib )

    # handle Qt5 output as the other standard CMake packages
    include( FindPackageHandleStandardArgs )
    find_package_handle_standard_args( Qt5ATLAS
        FOUND_VAR Qt5ATLAS_FOUND
        REQUIRED_VARS QtCore_location
        VERSION_VAR _version
    )
endif()

# Set up the RPM dependency:
lcg_need_rpm( Qt5 FOUND_NAME Qt5 VERSION_NAME QT5 )
