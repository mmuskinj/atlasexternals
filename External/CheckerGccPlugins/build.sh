#!/bin/sh

# Simple stand-alone build script for CheckerGccPlugins.
# Builds in the current directory.
# Argument should be path to the CheckerGccPlugins directory.

CHECKER_ROOT=$1
if [ "$CHECKER_ROOT" = "" ]; then
    echo "Usage: $0 <path to CheckerGccPlugins directory>"
    exit 1
fi

SRCS=`ls $CHECKER_ROOT/src/*cxx`
OBJS=${SRCS//.cxx/.o}

OBJS=""
for f in $SRCS; do
    gcc -fPIC -O2 -c -fno-rtti -iwithprefix plugin/include $f
    OBJS="$OBJS `basename $f .cxx`.o"
done

g++ -O2 -shared -o libchecker_gccplugins.so $OBJS

for test in $CHECKER_ROOT/test/*cxx; do
    case `basename $test .cxx` in
        usingns* ) ;;
        thread10 ) ;;
        * )
            echo $test
            testbase=`basename $test .cxx`
            LANG=C gcc -Wall -c -o /dev/null -std=c++14 -fplugin=libchecker_gccplugins.so -fplugin-arg-libchecker_gccplugins-checkers=all $test &> $testbase.log
            export testStatus=$?
            $CHECKER_ROOT/test/post.sh $testbase "" $CHECKER_ROOT/share/$testbase.ref
        ;;
    esac
done
